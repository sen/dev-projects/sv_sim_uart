module uart_fsm #(
    parameter          UART_CLK_FREQ = 50_000_000,
    parameter          UART_BAUD     = 115_200
) (
    // System Signals
    input  wire    n_reset,
    input  wire    clk,
    // UART signals
    input  wire    uart_rxd,
    output wire    uart_txd
);

// uart signals
    wire [7:0]   rx_data;
    wire         rx_avail;
    wire         rx_error;
    wire [7:0]   tx_data;
    wire         tx_wr;
    wire         tx_busy;

// uart instance
// TODO: Maybe smoehow use the error flag
uart #( .freq_hz (UART_CLK_FREQ), .baud (UART_BAUD) )
i_uart (
    .n_reset  ( n_reset  ),
    .clk      ( clk      ),
    .uart_rxd ( uart_rxd ),
    .uart_txd ( uart_txd ),
    .rx_data  ( rx_data  ),
    .rx_avail ( rx_avail ),
    .rx_error ( rx_error ),
    .rx_ack   (          ),
    .tx_data  ( tx_data  ),
    .tx_wr    ( tx_wr    ),
    .tx_busy  ( tx_busy  )
);

logic [3:0] cpt;


enum logic[1:0] {IDLE, WRITE, NEXT} state;

always_ff @ (posedge clk or negedge n_reset)
if (!n_reset)
begin
  state   <= IDLE;
  cpt     <= '0;
end
else case(state)
  IDLE: begin
    cpt <= '0;
    if(!tx_busy) state <= WRITE;
  end
  WRITE:
    if (cpt == 4'hf) state <= IDLE;
    else state <= NEXT;
  NEXT: if (!tx_busy) begin
    state <= WRITE;
    cpt <= cpt + 1'b1;
  end
endcase

assign tx_data = "a" + cpt;
assign tx_wr = (state == WRITE);

endmodule
