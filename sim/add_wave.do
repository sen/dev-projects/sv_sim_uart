add wave -noupdate -divider {UART Interface}
add wave -noupdate /uart_tb/uart/rx
add wave -noupdate /uart_tb/uart/tx
add wave -noupdate -divider {Design Under Test}
add wave -noupdate /uart_tb/DUT/*
