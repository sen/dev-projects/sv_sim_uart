interface uart_ifs();

   logic tx;
   logic rx;

   modport device    ( input rx, output tx);
   modport testbench ( output rx, input tx);

endinterface

