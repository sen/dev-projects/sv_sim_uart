module uart_tb();

uart_ifs uart();

wire uart_rxd;
wire uart_txd;

bit clk, n_reset;

assign uart_rxd = uart.rx;
assign uart.tx = uart_txd;

uart_tester tester_i(.*);

uart_fsm DUT(.*);

always #10ns clk = !clk;

endmodule


program uart_tester(uart_ifs.testbench uart, input clk, output bit n_reset);
  import uart_driver_pkg::*;

  uartDriver driver;

  string  test_seq = "helloworld\n";


  initial
  begin
    driver = new(uart);

    n_reset = 0;
    repeat(13) @(negedge clk);
    n_reset = 1;
    repeat(13) @(negedge clk);

    fork
    begin
      foreach(test_seq[i])
        driver.write(test_seq[i]);
    end
    begin
      forever
        driver.read();
    end
    join_any
  end

endprogram
