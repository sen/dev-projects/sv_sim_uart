package uart_driver_pkg;

  import uart_conf_pkg::*;
  export uart_conf_pkg::UART_DATABITS;

  localparam time UART_BAUD_P = (1_000_000_000ns/UART_BAUDRATE);

  // UART driver class
  class uartDriver;

    virtual uart_ifs.testbench uart;

    logic[UART_DATABITS-1:0] read_data;

    // costructor
    function new( virtual uart_ifs.testbench i);
      this.uart = i;
      this.read_data = '0;
    endfunction

    task reset;
      uart.rx = 1;
    endtask

    task write(input[UART_DATABITS-1:0] data);
    begin
      $info("UART sending %h (%c)",data,data);
      // start bit
      uart.rx = 0;
      #(UART_BAUD_P);
      for (int i=0;i<UART_DATABITS;i=i+1)
      begin
        uart.rx = data[i];
        #(UART_BAUD_P);
      end
      // parity
      if (UART_PARITY != PARITY_NONE)
      begin
        int p = $countones(data);
        if (UART_PARITY == PARITY_EVEN)
          uart.rx = p%2 ? 1 : 0;
        else
          uart.rx = p%2 ? 0 : 1;
        #(UART_BAUD_P);
      end
      // stop bits
      repeat(UART_STOPBITS)
      begin
        uart.rx = 1;
        #(UART_BAUD_P);
      end
    end
    endtask

    task read();
      // wait for start bit
      while (uart.tx != 0) #(UART_BAUD_P/16);
      for (int i=0;i<UART_DATABITS;i=i+1)
      begin
        #(UART_BAUD_P);
        read_data[i] = uart.tx;
      end
      // check parity
      if (UART_PARITY != PARITY_NONE)
      begin
        int p;
        #(UART_BAUD_P);
        p = $countones({uart.tx,read_data});
        if ((UART_PARITY == PARITY_EVEN && p%2 == 1) || (UART_PARITY == PARITY_ODD && p%2 == 0))
          $warning("UART: Wrong parity");
      end
      // check sotp bits
      repeat(UART_STOPBITS)
      begin
        #(UART_BAUD_P);
        if (uart.tx != 1) $warning ("Wrang frame format!");
      end

      $info("UART received %h (%c)",read_data, read_data);
    endtask

  endclass


endpackage
