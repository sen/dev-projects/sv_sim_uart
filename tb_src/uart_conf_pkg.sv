package uart_conf_pkg;

  typedef enum {PARITY_NONE, PARITY_EVEN, PARITY_ODD} parity_t;
  localparam UART_BAUDRATE = 115200;
  localparam UART_DATABITS = 8;
  localparam UART_STOPBITS  = 1;
  localparam parity_t UART_PARITY   = PARITY_NONE;

endpackage
